

Original general objective
	To provide tools that ease a reflective language evolution, in particular, to enable the generation of small and custom Language Kernels for IoT devices. This kind of devices are usually constrained with regard to its hardware characteristics (storage and buffers size, limited power source, processing power). Besides, they are intended to play a specific role in the IoT network (as terminal nodes: sensors/actuators), which implies that the applications they run will usually demand only a limited set of functionalities from its host language. According to this context, providing a way of generating custom language kernels that contain the minimum set of functionalities required by an specific application, has great value. 

	Due to its minimalistic nature, the kernels can't afford to provide functionalities that escape its original objective. In most cases, functionalities such as an I/O interface or debugging support won't be supported by the kernel, difficulting the interaction with the system. This limitation is particularly relevant if the system doesn't behave in the expected way or if it crashes, ending up in a corrupted state from which it cannot recover by itself.

	Given the previous scenario we identify two questions: How to define the minimum set of components to be part of the kernel. How to debug a kernel that doesn't provide debugging support and may be in a corrupted state. 

	(TODO: explain that a kernel is composed of a subset of components from an original language)


About the current bootstrap process
	Kernels of Reflective languages are generated via bootstrap process. The bootstrap is a technique in which a compiler written in the same language it compiles, generates the language definition. The reflective Pharo language is currently defined by a bootstrap process that runs in the same language and which generates a full Pharo image from its source code. This process determines the contents of the language and the order in which its components must be loaded. This last step is fundamental to deal with the metacircularities proper of reflective systems.

	The current version of the Pharo bootstrap is carefully tunned to work only for this particular configuration of the system. It solves the metacircularities of the language by creating temporary stubs for the required basic objects, and by precisely defining the order in which each component of the system is loaded.


	The bootstrap process is split in 2 parts: (see figure, add explanation for the 2 parts in the figure or text)
		- Bootstrap a minimal image (currently includes 72 (TODO: find the exact number) packages) using Espell: which uses VM-Simulator and an AST interpreter
		- Load the rest of the packages using Hermes (TODO: explain what is hermes, how many packages does it load).
			Hermes is used to grow the minimal image passing by the next steps:
				- Minimal 30M (before the first time it's executed by the vm)
				- Compiler 4.1M
				- Traits 4.4M
				- Core 4.6M
				- Monticello 7.1M 
				- Metacello 8.5M

	The current bootstrap process in Pharo, as a first step, generates a "minimal image" that contains a subset of the components of the full language definition. The current granularity for the components is the package. 


About the components in a kernel
	A component is a delimited part of the language definition. It can be a package, a class, etc. The presence or absense of a component affects the functionalities provided by the generated system. Components are interrelated among themselves through different kinds of dependencies. If a component doesn't get its dependencies fulfilled, its behavior turns impredictable. (TODO: is this true?)

	For reflective languages, the design of the language itself imposes restrictions on the minimum set of components that a kernel of the language must contain to mantain the integrity of its metamodel. For example, among the classes that cannot be missing, there are the base classes (#SmallInteger, #Character, #ByteSymbol ... etc), and also the classes that represent reification aspects of the language (#ClassVariable (package Slot-Core))

	There is another set of components that contain functionalities required by the bootstrap process itself to work. For example: classes with the necessary code to install classes: #ShiftClassInstaller. This kind of components must be available during the bootstrap process but they could be excluded from the generated kernel. (TODO: To prove this in an experiment)

	The generation of a language kernel must be goal oriented: a custom kernel should provide the minimum set of functionalities for a specific application to run. Defining the corresponding set of components is not trivial. (TODO: Explain why this is not trivial)

Ideas for determining the components for a kernel:


		It would be useful to use a set of tests associated to the application. An image in which all tests are green contains the set or a superset of the required components. 
			There are two options for getting the correct combination of components:
			- Genetic algorithm
						- Genetic algorithm based on a test base that defines the requirements of a kernel, and another set of tests that define the requirements for a custom application.

			- Run the tests using a custom AST interpreter which performs the lookup in the classes of the full system (or in a ring model that represents the full system) and marks the classes / methods used during the tests excecution.




About interacting with a minimal system
	A minimal system that runs an application on top of a custom language kernel, won't provide debugging support and may not even support I/O operations, these conditions make difficult to interact with the system, which is specially relevant at the moment of debugging an application or when the system crashes and it finishes in an invalid state. 

	In Pharo, a minimal system is stored as bytecode in an image file, which is loaded and executed by a Virtual Machine. If the system gets corrupted, the Virtual Machine won't be able to excecute the image. (TODO: in which ways the image could get corrupted? specify scenarios)

Ideas for supporting debugging in a minimal system
	The full debugging infrastructure should be provided from a full Pharo system. The system will be build on top of Espell, one of the tools used to perform the current bootstrap process in Pharo.

	The first challenge is to load and create a model of the system inside the host. The nexts are the steps to follow (Figure2):
	- Read the raw bytecode from the minimal system image and load it as an object in the host system. 
	- (Challenge) Get the address, in the bytecode, of every object in the image, creating a representation of it, called "mirror" . And also get the execution stack trace of every executing process.
	- (Challenge) The system's source code is not saved in the image, therefore, it is necessary to define a way to reference and recover the source code installed in the image (possible complication: the version of the code may vary)
	- Generate a ring model using the recovered source code.

	A debugger will provide the infrastructure to explore and manipulate the minimal system. It will excecute code using a custom AST interpreter, which will perform the lookup of functions on the ring model and will reference the objects in the system, using the object's mirror representation.

	An important challenge is to mantain the mirror's references updated as the system excecutes. 

	The possible ways in which the debugger will be able to modify the minimal system are not determined yet. For example, if the system failed because a component was missing, will the debugger be able to load the component?. If the problem was a bug in an application, how will the new code be loaded? how to mantain the consistency of the system if, for example, a class is deleted but there were instances associated to it? 



---------------------------------------------------------------------------------------------------------

Raw notes:

Challenges:
-Regarding the definition of a language Kernel
	
	- Detecting and treating Metamodel inconsistencies
		Meta-circularities
			- As we are bootstrapping a reflective language, the bootstrap process will require the presense of some components that are fundamental for the reification of the language to be able to complete the process. If these components are missing or if they are not loaded in the correct order, the bootstrap process will fail before completing the generation of the language definition.
				
		Dependency inconsistency
			Example1: inconsistency in the inheritance chain may lead to problems during the bootstrap itself or at runtime once the minimal system is executing

- Regarding the bootstrap (technical)
	- Bootstrapping a new kernel definition is a fallible process, since dependencies can fail (or for other reasons). 
	- A bootstrapped system be unable to execute code. In some cases the system will stay in an inconsistent state from which it can't recover.
	- Thus, it's necessary to be able to debug the bootstrap process.
	- Since the small language kernel is limited, it doesn't provide debugging support.


- Regarding corrupted images in Pharo
	- Excecution issues (program bugs)
		Example: method code contains a bug that shows in excecution time 
		Example: image is corrupted during excecution

Side ideas:
(Mention the fact that some languages include debugging support in their VMs)

Engineering:
	Infrastructure for providing debug support
	Alternatives:
	- Excecution of code directly in the bytecode using VM-Maker (VM simulator)
	- Interpretation of code using a Debuggable AST interpreter
	Having a debugger that runs code in a "dead image" would allow to:
		-  Load and manipulation of a corrupted image (not necessarly a small image) from host image. (look for examples of corrupted images, ask why an image could get corrupted)
		- The latter is a possible solution to a known problem of Reflective systems evolving: the impossibility to recover from system crashed 

